// Fill out your copyright notice in the Description page of Project Settings.

#include "undercakes.h"
#include "CakeItem.h"


// Sets default values
ACakeItem::ACakeItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ACakeItem::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ACakeItem::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}