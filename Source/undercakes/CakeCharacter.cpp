// Fill out your copyright notice in the Description page of Project Settings.

#include "undercakes.h"
#include "CakeCharacter.h"

// Auto-Magic stuff from Unreal
ACakeCharacter::ACakeCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
}
void ACakeCharacter::BeginPlay()
{
	Super::BeginPlay();
}
void ACakeCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
}
void ACakeCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
}



// Custom Stuff
void ACakeCharacter::AffectHealth(float Delta)
{
	Health += Delta;
	UpdateIsDead();
}

void ACakeCharacter::UpdateIsDead()
{
	isDead = Health <= 0 ? true : false;
}

void ACakeCharacter::LevelUp()
{
	StatLevel++;
	StatAgi += StatAgiPerLevel;
	StatDex += StatDexPerLevel;
	StatEnd += StatEndPerLevel;
	StatStr += StatStrPerLevel;
	StatHealthMax += StatHealthPerLevel;
	StatRegenRate += StatRegenPerLevel;
}

void ACakeCharacter::PickupItem()
{
	// Pickup some CakeItem and add it to the inventory. It could be:
	// Weapon (add to weapon list)
	// Powerup (instant)
	// Upgrade (Add to list of active stat affectors)
	// Add a weapon to the weapon list
}

void ACakeCharacter::DropItem()
{
	// Drop an item into space
}