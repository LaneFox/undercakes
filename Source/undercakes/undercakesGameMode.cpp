// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "undercakes.h"
#include "undercakesGameMode.h"
#include "undercakesPlayerController.h"
#include "undercakesCharacter.h"

AundercakesGameMode::AundercakesGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AundercakesPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}