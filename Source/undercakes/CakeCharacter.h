// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "CakeCharacter.generated.h"


UCLASS(Blueprintable)
class UNDERCAKES_API ACakeCharacter : public ACharacter
{
	GENERATED_BODY()

public:

	//
	//
	// VARIABLES
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "CakeCharacter") float Health = 100;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "CakeCharacter") bool isDead = false;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") FString Title = FString(TEXT("Name"));

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int TeamId = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatAgi = 10;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatStr = 10;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatDex = 10;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatEnd = 10;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatHealth = 100;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatHealthMax = 100;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatRegenRate = 0.5;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int StatLevel = 1;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatAgiPerLevel = 0.1;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatStrPerLevel = 3;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatDexPerLevel = 0.1;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatEndPerLevel = 2;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatHealthPerLevel = 30;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") float StatRegenPerLevel = 0.4;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int Score = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int Kills = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int Deaths = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int DamageDealt = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int DamageTaken = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int Currency = 0;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int XpOnKillReward = 100;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = "CakeCharacter") int XpCurrent = 0;

	//
	//
	// PUBLIC FUNCTIONS
	UFUNCTION(BlueprintCallable, Category = "CakeCharacter") virtual void LevelUp();
	UFUNCTION(BlueprintCallable, Category = "CakeCharacter") virtual void PickupItem();
	UFUNCTION(BlueprintCallable, Category = "CakeCharacter") virtual void DropItem();
	UFUNCTION(BlueprintCallable, Category = "CakeCharacter") virtual void AffectHealth(float delta);

	//
	//
	// ETC

	// Built In
	ACakeCharacter();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Custom
	virtual void UpdateIsDead();

};