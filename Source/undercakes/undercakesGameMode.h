// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "undercakesGameMode.generated.h"

UCLASS(minimalapi)
class AundercakesGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AundercakesGameMode();
};



